<?php

namespace App\Providers;

use App\Services\ProjectService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;
use App\Services\TaskService;
use App\Services\StepService;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProjectService::class);
        $this->app->bind(TaskService::class);
        $this->app->bind(StepService::class);
        $this->app->bind(UserService::class);
    }
}
