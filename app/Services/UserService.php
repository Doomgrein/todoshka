<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 04.11.2018
 * Time: 2:03
 */
namespace App\Services;

use App\User;

class UserService
{
    /**
     * @param int $id
     * @return mixed
     */
    public function toggleStatus($id)
    {
        $user = User::find($id);
        $user->is_admin = !$user->is_admin;
        $user->save();
        return $user;
    }
}
