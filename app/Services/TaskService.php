<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 21.09.2018
 * Time: 23:20
 */
namespace App\Services;

use App\Models\Task;

class TaskService
{
    /**
     * @param array $fields
     * @return Task
     */
    public function create(array $fields)
    {
        $task = new Task;
        $task->fill($fields);
        $task->save();
        return $task;
    }

    /**
     * @param int $id
     * @param array $fields
     * @return mixed
     */
    public function update(int $id, array $fields)
    {
        $task = Task::find($id);
        $task->fill($fields);
        $task->save();
        return $task;
    }
}