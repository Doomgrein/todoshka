<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 12.09.2018
 * Time: 23:24
 */
namespace App\Services;

use App\Models\Project;

class ProjectService
{
    /**
     * @param array $fields
     * @return Project
     */
    public function create(array $fields)
    {
        $project = new Project;
        $project->fill($fields);
        $project->save();
        return $project;
    }

    /**
     * @param int $id
     * @param array $fields
     * @return Project
     */
    public function update(int $id, array $fields)
    {
        /** @var Project $project */
        $project = Project::find($id);
        $project->fill($fields);
        $project->save();
        return $project;
    }

    /**
     * @param int $id
     */
    public function toggleStatus(int $id)
    {
        /** @var Project $project */
        $project = Project::find($id);
        $project->is_active = !$project->is_active;
        $project->save();
        return $project;
    }
}