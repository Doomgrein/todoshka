<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 16.10.2018
 * Time: 0:29
 */
namespace App\Services;

use App\Models\Step;

class StepService
{
    /**
     * @param array $fields
     * @return Step
     */
    public function create(array $fields)
    {
        $step = new Step;
        $step->fill($fields);
        $step->save();
        return $step;
    }

    /**
     * @param int $id
     * @param array $fields
     * @return mixed
     */
    public function update(int $id, array $fields)
    {
        $step = Step::find($id);
        $step->fill($fields);
        $step->save();
        return $step;
    }
}