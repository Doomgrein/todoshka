<?php

namespace App\Http\Controllers\Admin;

use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::paginate(5);
        return view('admin.users.index', [
            'users' => $users
        ]);
    }

    /**
     * @param UserService $userService
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggleStatus(UserService $userService, $id)
    {
        $userService->toggleStatus($id);
        return redirect()->route('admin.users.index');
    }
}
