<?php

namespace App\Http\Controllers\API;

use App\Models\Step;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class StepController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function getSteps(Request $request)
    {
        $project_id = $request->get('project_id');
        $steps = Step::where('project_id', $project_id)->get();
        return $steps;
    }
}
