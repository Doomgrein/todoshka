<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\Request;

use App\Http\Requests;

class IndexController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $projects = Project::paginate(10);
        return view ('welcome', ['projects' => $projects]);
    }
    // Не использовать select
    public function showProject($id)
    {
        $project = Project::find($id);
        return view ('project')->with([
            'project' => $project,
            ]);
    }
}
