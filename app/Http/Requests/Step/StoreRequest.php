<?php

namespace App\Http\Requests\Step;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() !== null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required','string'],
            'project_id' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Укажите название этапа',
            'project_id.required' => 'Выберите проект'
        ];
    }
}
