<?php

namespace App\Http\Requests\Task;

use App\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user() !== null;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required','string'],
            'performer' => ['required','string'],
            'project_id' => ['required', 'numeric', 'min:1'],
            'step_id' => ['required', 'numeric', 'min:1']
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Укажите название задачи',
            'performer.required' => 'Укажите, пожалуйста, исполнителя',
            'project_id.min' => 'Выберите проект',
            'step_id.min' => 'Выберите этап'
        ];
    }
}
