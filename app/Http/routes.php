<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'IndexController@index');

Route::get('project/{id}', 'IndexController@showProject')->name('projectShow');

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/test', 'TestController@testMethod');

Route::group(['prefix' => 'admin', 'namespace' => 'API'], function (){
    Route::get('tasks/create/json-steps', 'StepController@getSteps');
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'AdminMiddleware'], function() {
    Route::resource('projects', 'ProjectController');
    Route::get('/', function () {
        return view('admin.index');
    });
    Route::resource('tasks', 'TaskController');
    Route::resource('steps', 'StepController');
    Route::get('users/index', 'UserController@index')->name('admin.users.index');
    Route::get('users/toggle/{toggle}', 'UserController@toggleStatus')->name('admin.users.toggle');
});
