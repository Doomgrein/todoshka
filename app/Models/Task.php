<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [ 'title', 'id', 'note', 'performer', 'costs', 'status', 'project_id', 'step_id' ];

    // Relations:
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function step()
    {
        return $this->belongsTo(Step::class);
    }

    public function performer()
    {
        return $this->belongsTo(User::class);
    }
}
