<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['title', 'description', 'id', 'status'];

    // Relations:
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    public function steps()
    {
        return $this->hasMany(Step::class);
    }
}
