@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">
                Задачи проекта <b>{{ $project->title }}</b>
            </div>

            <div class="panel-body">
                <table class="table table-striped task-table" border="1">

                    <!-- Заголовок таблицы -->
                    <thead>
                        <tr style="background: lightgoldenrodyellow">
                            <th>Задача</th>
                            <th>Примечание</th>
                            <th>Исполнитель</th>
                            <th>Трудозатраты</th>
                            <th>Статус</th>
                        </tr>
                    </thead>

                                    <!-- Тело таблицы -->
                    <tbody>
                    @foreach($project->steps as $step)
                        <tr style="background: lightskyblue">
                            <td colspan="5">
                                <i><b><u>{{$step->title}}</u></b></i>
                            </td>
                        </tr>
                        @foreach ($step->tasks as $task)
                            <tr>
                                    <!-- Имя задачи -->
                                    <td class="table-text">
                                        <div>{{ $task->title }}</div>
                                    </td>

                                    <td class="table-text">
                                        <div>{{ $task->note }}</div>
                                    </td>

                                    <td class="table-text">
                                        <div>{{ $task->performer }}</div>
                                    </td>

                                    <td class="table-text">
                                        <div>{{ $task->costs }}</div>
                                    </td>

                                    <td class="table-text">
                                        <div>{{ $task->status }}</div>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection