@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content')
    <div class="container pull-left col-md-5">
        <div class="box col-md-1">
            {{ Form::open(array('route' => ['admin.steps.update', $step->id], 'method' => 'put')) }}
            <div class="box-header with-border">
                <h3 class="box-title">Редактируем этап</h3>
                @include('admin.errors')
            </div>
            <div class="box-body">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Название этапа</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="title" value="{{ old('title', $step->title)}}">
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button class="btn btn-warning">Изменить</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop