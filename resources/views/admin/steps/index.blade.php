@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
    <h1>TODOшка</h1>
@stop

@section('content')
    <div class="container pull-left col-md-3">
        <div class="row">
            <div class="panel panel-default">
                <div class="box-body">
                    <table class="table table-bordered table-stripped">
                        <thead>
                        <tr>
                            <th>Название этапа</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <div class="form-group">
                            <a href="{{route('admin.steps.create')}}" class="btn btn-success">Добавить этап</a>
                        </div>
                        @foreach($projects as $project)
                            <tr style="background: lightskyblue">
                                <td colspan="2">
                                    <h4><i><b>{{$project->title}}</b></i></h4>
                                </td>
                            </tr>
                            @foreach ($project->steps as $step)
                                <tr style="background: lightyellow">
                                    <td colspan="1">
                                        <i><b>{{$step->title}}</b></i>
                                    </td>

                                    <td>
                                        <a href="{{ route('admin.steps.edit', $step->id) }}" class="fa fa-pencil"></a>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="delete" data-toggle="modal" data-target="#myModal{{ $step->id }}">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="myModal{{ $step->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Подтверждение</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        Вы уверены?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                                        {{Form::open(['route' => ['admin.steps.destroy', $step->id], 'method' => 'delete'])}}
                                                        <button type="submit" class="btn btn-primary">Да</button>
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop