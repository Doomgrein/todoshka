@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
    <h1>TODOшка</h1>
@stop

@section('content')
    <div class="container pull-left col-md-4">
        <div class="row">
            <div class="panel panel-default">
                <div class="box-body">
                    <table class="table table-bordered table-stripped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Имя пользователя</th>
                            <th>Статус</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>
                                    @if ($user->is_admin == '1')
                                        <b style="color:#32CD32">Администратор</b>
                                    @else
                                        <b style="color:#DC143C">Пользователь</b>
                                    @endif</td>
                                <td>
                                    @if ($user->is_admin == '1')
                                        <a href="{{ route('admin.users.toggle', $user->id) }}" class="fa fa-arrow-circle-down"></a>
                                    @else
                                        <a href="{{ route('admin.users.toggle', $user->id) }}" class="fa fa-arrow-circle-up"></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
    </div>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop