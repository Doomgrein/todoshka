@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
    <h1>TODOшка</h1>
@stop

@section('content')
    <div class="container pull-left col-md-8">
        <div class="row">
            <div class="panel panel-default">
                <div class="box-body">
                    <table class="table table-bordered table-stripped">
                        <thead>
                        <tr>
                            <th>Название задачи</th>
                            <th>Примечание</th>
                            <th>Исполнитель</th>
                            <th>Трудозатраты</th>
                            <th>Статус</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <div class="form-group">
                            <a href="{{route('admin.tasks.create')}}" class="btn btn-success">Добавить задачу</a>
                        </div>
                        @foreach ($projects as $project)
                            <tr style="background: lightskyblue">
                                <td colspan="7">
                                    <h4><i><b>{{$project->title}}</b></i></h4>
                                </td>
                            </tr>
                            @foreach($project->steps as $step)
                                <tr style="background: lightyellow">
                                    <td colspan="7">
                                        <i><b>{{$step->title}}</b></i>
                                    </td>
                                </tr>
                                @foreach($step->tasks as $task)
                                    <tr>

                                        <td class="table-text">
                                            <div>{{ $task->title }}</div>
                                        </td>

                                        <td class="table-text">
                                            <div>{{ $task->note }}</div>
                                        </td>

                                        <td class="table-text">
                                            <div>{{ $task->performer }}</div>
                                        </td>

                                        <td class="table-text">
                                            <div>{{ $task->costs }}</div>
                                        </td>

                                        <td class="table-text">
                                            <div>{{ $task->status }}</div>
                                        </td>

                                        <td>
                                            <a href="{{ route('admin.tasks.edit', $task->id) }}" class="fa fa-pencil"></a>
                                            <!-- Button trigger modal -->
                                            <button type="button" class="delete" data-toggle="modal" data-target="#myModal{{ $task->id }}">
                                                <i class="fa fa-remove"></i>
                                            </button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal{{ $task->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Подтверждение</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            Вы уверены?
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                                            {{Form::open(['route' => ['admin.tasks.destroy', $task->id], 'method' => 'delete'])}}
                                                            <button type="submit" class="btn btn-primary">Да</button>
                                                            {{ Form::close() }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                        @endforeach
                        </tbody>
                    </table>
                    {{ $projects->links() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop