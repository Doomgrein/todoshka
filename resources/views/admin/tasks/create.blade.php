@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content')
    <div class="container pull-left col-md-5">
        <div class="box col-md-1">
            {{ Form::open(array('route' => ['admin.tasks.store'])) }}
            <div class="box-header with-border">
                <h3 class="box-title">Создаем задачу</h3>
                @include('admin.errors')
            </div>
            <div class="box-body">
                <div class="col-md-8">
                    <div class="form-group">
                        <div class="form-group">
                            <label for="">Ваши проекты</label>
                            <select class="form-control" name="project_id" id="projects">
                                <option value="0" disable="true" selected="true"> Выберите проект </option>
                                @foreach ($projects as $project)
                                    <option @if ( old('project_id') == $project->id) selected @endif value="{{ $project->id }}">{{ $project->title }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Ваши этапы</label>
                            <select class="form-control" name="step_id" id="steps">
                                <option value="0" disable="true" selected="true"> Выберите этап </option>
                            </select>
                        </div>
                        <label for="exampleInputEmail1">Название задачи</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="title" value="{{ old('title') }}">
                        <label for="exampleInputEmail1">Примечание</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="note" value="{{ old('note') }}">
                        <label for="exampleInputEmail1">Исполнитель</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="performer" value="{{ old('performer') }}">
                        <label for="exampleInputEmail1">Трудозатраты</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="costs" value="{{ old('costs') }}">
                        <label>Статус</label>
                        {{ Form::select('status',
                         [
                         'Новое' => 'Новое',
                         'Запланировано' => 'Запланировано',
                         'Решено' => 'Решено',
                         'Отложено' => 'Отложено',
                         'Отклонено' => 'Отклонено'
                         ],
                         null,
                         ['class' => 'form-control select2', 'data-placeholder'=>'Статус']
                         ) }}
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button class="btn btn-success">Добавить</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
@stop

@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bo otstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $('#projects').on('change', function(e){
           var project_id = e.target.value;
           $.get('/admin/tasks/create/json-steps?project_id=' + project_id,function(data) {
               $('#steps').empty();
               $('#steps').append('<option value="0" disable="true" selected="true"> Выберите этап </option>');

               $.each(data, function(index, stepsObj){
                   $('#steps').append('<option value="'+ stepsObj.id +'"> '+ stepsObj.title +' </option>');
               });
           });
        });
    </script>
@stop