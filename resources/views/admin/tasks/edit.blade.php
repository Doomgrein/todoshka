@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content')
    <div class="container pull-left col-md-5">
        <div class="box col-md-1">
            {{ Form::open(array('route' => ['admin.tasks.update', $task->id], 'method' => 'put')) }}
            <div class="box-header with-border">
                <h3 class="box-title">Редактируем задачу</h3>
                @include('admin.errors')
            </div>
            <div class="box-body">
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Название задачи</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="title" value="{{ old('title', $task->title) }}">
                        <label for="exampleInputEmail1">Примечание</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="note" value="{{ old('note', $task->note) }}">
                        <label for="exampleInputEmail1">Исполнитель</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="performer" value="{{ old('performer', $task->performer) }}">
                        <label for="exampleInputEmail1">Трудозатраты</label>
                        <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="costs" value="{{ old('costs', $task->costs) }}">
                        <label for="exampleInputEmail1">Статус</label>
                        {{ Form::select('status',
                         [
                         'Новое' => 'Новое',
                         'Запланировано' => 'Запланировано',
                         'Решено' => 'Решено',
                         'Отложено' => 'Отложено',
                         'Отклонено' => 'Отклонено'
                         ],
                         $task->status,
                         ['class' => 'form-control select2', 'data-placeholder'=>'Статус']
                         ) }}
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button class="btn btn-warning">Изменить</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop