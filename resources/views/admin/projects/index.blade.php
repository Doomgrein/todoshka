@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
    <h1>TODOшка</h1>
@stop

@section('content')
    <div class="container pull-left col-md-4">
        <div class="row">
                <div class="panel panel-default">
                    <div class="box-body">
                        <div class="form-group">
                            <a href="{{ route('admin.projects.create') }}" class="btn btn-success">Добавить проект</a>
                        </div>
                        <table class="table table-bordered table-stripped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Название проекта</th>
                                    <th>Статус</th>
                                    <th>Действия</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <tr>
                                    <td>{{ $project->id }}</td>
                                    <td>{{ $project->title }}</td>
                                    <td>
                                    @if ($project->status == 'Активно')
                                        <b style="color:#32CD32">{{ $project->status }}</b>
                                    @else
                                        <b style="color:#DC143C">{{ $project->status }}</b>
                                    @endif</td>
                                    <td>
                                        <a href="{{ route('admin.projects.edit', $project->id) }}" class="fa fa-pencil"></a>
                                        <!-- Button trigger modal -->
                                        <button type="button" class="delete" data-toggle="modal" data-target="#myModal{{ $project->id }}">
                                            <i class="fa fa-remove"></i>
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="myModal{{ $project->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">Подтверждение</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        Вы уверены?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                                                        {{ Form::open(['route' => ['admin.projects.destroy', $project->id], 'method' => 'delete']) }}
                                                            <button type="submit" class="btn btn-primary">Да</button>
                                                        {{ Form::close() }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $projects->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop