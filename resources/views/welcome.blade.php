@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">TODOшка</div>

                <div class="panel-body">
                    @foreach ($projects as $project)
                        <a href="{{ route('projectShow', $project->id) }}" role="button">
                            <div class="col md-2 table-bordered col-md-12">
                                <h2>{{$project->title}}</h2>
                                <p>{{$project->description}}</p>
                                Статус:
                                    @if ($project->status == 'Активно')
                                        <b style="color:#32CD32">{{ $project->status }}</b>
                                    @else
                                        <b style="color:#DC143C">{{ $project->status }}</b>
                                    @endif
                            </div>
                        </a>
                    @endforeach
                    {{ $projects->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
